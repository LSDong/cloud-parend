package com.qf.cloudjifen.interceptor;

import com.alibaba.csp.sentinel.adapter.spring.webmvc.callback.RequestOriginParser;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;


//被调用方需要获取请求头
@Component
public class CustomerRequestOriginParser implements RequestOriginParser{
    @Override
    public String parseOrigin(HttpServletRequest request) {
        return request.getHeader("source");
    }
}
