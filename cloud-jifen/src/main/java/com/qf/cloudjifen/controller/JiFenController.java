package com.qf.cloudjifen.controller;

import com.qf.cloudentity.entity.JiFen;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("jifen")
@RefreshScope
public class JiFenController {

//    @Value("${name}")
//    String name;
//
//
//    @Value("${age}")
//    Integer age;

    //@Value("${blog}")
    //String blog;

    @PostMapping(value = "save")
    public Map<String,Object> save(@RequestBody JiFen jiFen) throws InterruptedException {
        System.out.println("调用了积分的保存接口");
        System.out.println(jiFen);
//        System.out.printf("从配置中心读取到的配置项是:name=%s,age=%d", name, age);


        //演示慢调用 休眠两秒钟
//        TimeUnit.SECONDS.sleep(2);

        //异常比例熔断
//        int a=1/0;
        return new HashMap(){{
           put("code",200);
           put("msg","积分保存成功");
        }};
    }
}
