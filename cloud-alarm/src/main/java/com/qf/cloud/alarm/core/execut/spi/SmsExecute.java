package com.qf.cloud.alarm.core.execut.spi;

import com.qf.cloud.alarm.core.execut.api.IExecute;
import com.qf.cloud.alarm.core.loader.entity.RegisterInfo;
import com.qf.cloud.alarm.core.loader.helper.RegisterInfoLoaderHelper;
import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.sms.v20210111.SmsClient;
import com.tencentcloudapi.sms.v20210111.models.SendSmsRequest;
import com.tencentcloudapi.sms.v20210111.models.SendSmsResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by yihui on 2018/4/10.
 */
public class SmsExecute implements IExecute {

    private static final Logger logger = LoggerFactory.getLogger("alarm");


    @Override
    public void sendMsg(List<String> users, String title, String msg) {
        // 发送短信
        try {
            RegisterInfo info = RegisterInfoLoaderHelper.load();
            Credential cred = new Credential(info.getSecretId(), info.getSecretKey());

            // 实例化一个http选项，可选，没有特殊需求可以跳过
            HttpProfile httpProfile = new HttpProfile();
            httpProfile.setReqMethod("POST");
            httpProfile.setEndpoint(info.getEndpoint());

            ClientProfile clientProfile = new ClientProfile();
            clientProfile.setSignMethod("HmacSHA256");
            clientProfile.setHttpProfile(httpProfile);
            SmsClient client = new SmsClient(cred, info.getRegion(), clientProfile);
            SendSmsRequest req = new SendSmsRequest();
            // 签名ID
            req.setSmsSdkAppId(info.getAppId());
            // 签名内容
            req.setSignName(info.getSign());
            // 模板ID
            req.setTemplateId(info.getTemplateId());
            String[] templateParamSet = {info.getAppName()};
            req.setTemplateParamSet(templateParamSet);
            /* 下发手机号码，采用 E.164 标准，+[国家或地区码][手机号]
             * 示例如：+8613711112222， 其中前面有一个+号 ，86为国家码，13711112222为手机号，最多不要超过200个手机号 */
            String[] phoneNumberSet = users.toArray(new String[0]);
            req.setPhoneNumberSet(phoneNumberSet);
            // 发送短信
            SendSmsResponse res = client.SendSms(req);
            System.out.println(res);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}
