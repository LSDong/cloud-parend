package test.java.com.qf.cloud.alarm.core;

import com.qf.cloud.alarm.core.AlarmWrapper;
import com.qf.cloud.alarm.core.EmailWrapper;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.HtmlEmail;
import org.junit.Test;

import java.net.URL;

/**
 * Created by yihui on 2017/4/28.
 */
public class AlarmTest {

    private String template = "<html><meta charset=utf-8>\n" +
            "\n" +
            "<style>\n" +
            "div.card {\n" +
            "  background-color:white; \n" +
            " box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\n" +
            "  text-align: center;\n" +
            "}\n" +
            "\n" +
            "div.header {\n" +
            "    background-color: #4CAF50;\n" +
            "    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);\n" +
            "    color: white;\n" +
            "    padding: 10px;\n" +
            "    font-size: 40px;\n" +
            "}\n" +
            "\n" +
            "div.container {\n" +
            "    padding: 10px;\n" +
            "}\n" +
            "</style>\n" +
            "\n" +
            "<div class=\"card\">\n" +
            "  <div class=\"header\">\n" +
            "    <h1>星期一</h1>\n" +
            "  </div>\n" +
            "\n" +
            "  <div class=\"container\">\n" +
            "    <p>2016.04.10</p>\n" +
            "  </div>\n" +
            "</div>\n" +
            "</html>";

    @Test
    public void sendMsg() throws InterruptedException {
        String key = "NPE";
        String title = "NPE异常";
        String msg = "出现NPE异常了!!!";
        AlarmWrapper.getInstance().sendMsg(key, title, msg);  // 微信报警
        // 不存在异常配置类型, 采用默认报警, 次数较小, 则直接部署出
        AlarmWrapper.getInstance().sendMsg("zzz", "不存在xxx异常配置", "报警嗒嗒嗒嗒");
        Thread.sleep(1000);
    }

    /**
     * fixme 在实际测试时，请修改 alarm.properties 文件中的配置信息
     */
    @Test
    public void testHtmlEmailSend() {
        try {
            // Create the attachment
            EmailAttachment attachment = new EmailAttachment();
            attachment.setURL(new URL("http://i.heyige.cn/images/logo.png"));
            attachment.setDisposition(EmailAttachment.ATTACHMENT);
            attachment.setDescription("公众号");
            attachment.setName("logo.jpg");

            HtmlEmail email = EmailWrapper.genEmailClient();
            // 添加附件
            email.attach(attachment);
            email.setSubject("alarm测试!");

            // set the html message
            email.setHtmlMsg(template);

            // set the alternative message
            email.setTextMsg("Your email client does not support HTML messages");

            email.addTo("1712725320@qq.com");
            String ans = email.send();
            System.out.println(ans);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void testMultiConfLoader() {
        AlarmWrapper.getInstance().sendMsg("XXX", "指定默认ConfLoader报警", "报警内容!", "输出：{3}");
        AlarmWrapper.getInstance().sendMsg("TTT", "SelfAlarmConfLoader报警", "报警内容!", "输出：{3}");
    }


    public static void main(String[] args) throws InterruptedException {
        // 测试异常升级的case
        // 计数 [1 - 2] 默认报警（即无日志） （其中 < 3 的是因为未达到下限, 采用的默认报警）
        // 计数 [3 - 4] 默认邮件报警（其中 < 5 采用的默认报警, 与下面的区别是报警用户）
        // 计数 [5 - 9] 邮件报警 （大于5小于10根据上升规则,还是选择邮件报警）
        // 计数 [10 - 19] 微信报警
        // 计数 [20 - 30] 短信报警
        // 计数 [31 -] 默认报警 （超过上限, 不报警）
        for (int i = 0; i < 40; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    AlarmWrapper.getInstance().sendMsg("YYY", "异常报警升级测试");
                }
            }).start();
        }


        Thread.sleep(1000 * 600);
    }

}
