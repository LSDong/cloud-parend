package com.qf.cloudentity.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TbGoods implements Serializable {
    private static final long serialVersionUID = -1965210068419222686L;

    /**
     * 商品ID
     */
    private Integer goodsId;
    /**
     * 商品库存数量
     */
    private Integer goodsStock;
    /**
     * 商品价格
     */
    private Double goodsPrice;
    /**
     * 商品名称
     */
    private String goodsName;
}
