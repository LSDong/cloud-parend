package com.qf.cloudentity.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TbOrder implements Serializable {
    private static final long serialVersionUID = 6949663891894551333L;

    /**
     * 订单ID
     */
    private String orderId;
    /**
     *下单数量
     */
    private Integer orderNum;
    /**
     * 订单的总金额
     */
    private Double orderAmount;
    /**
     * 商品的ID
     */
    private Integer goodsId;

}
