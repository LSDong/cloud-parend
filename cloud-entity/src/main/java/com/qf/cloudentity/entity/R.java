package com.qf.cloudentity.entity;

import lombok.Data;

@Data
public class R<T> {
    // 请求成功的状态码
    public static Integer SUCCESS = 200;
    // 请求失败的状态码
    public static Integer FAIL = -1;

    // 返回的状态码
    private Integer code;

    // 提示消息
    private String msg;

    // 后端返回的真正数据
    private T data;

    private R(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    // 成功的响应
    public static <T> R<T> ok(Integer code, String msg, T data) {
        return new R<T>(code, msg, data);
    }

    // 成功的响应
    public static <T> R<T> ok(T data) {
        return ok(SUCCESS, "请求成功", data);
    }

    // 成功的响应
    public static <T> R<T> ok() {
        return ok(null);
    }

    // 失败的响应
    public static <T> R<T> fail(Integer code,String msg,T data){
        return new R<>(code, msg, data);
    }

    // 失败的响应
    public static <T> R<T> fail(T data){
        return fail(FAIL, "请求失败", data);
    }

    // 失败的响应
    public static <T> R<T> fail(){
        return fail(null);
    }

}
