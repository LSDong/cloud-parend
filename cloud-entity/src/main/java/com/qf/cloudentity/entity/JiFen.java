package com.qf.cloudentity.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JiFen {

    private Integer jiFenId;

    private Integer count;

    private String type;
}
