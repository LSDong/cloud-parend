package com.qf.cloudorders.advice;

import com.alibaba.csp.sentinel.slots.block.authority.AuthorityException;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeException;
import com.alibaba.csp.sentinel.slots.block.flow.FlowException;
import com.qf.cloud.alarm.core.AlarmWrapper;
import com.qf.cloudentity.entity.R;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class MyControllerAdvice {

    @ExceptionHandler(FlowException.class)
    public R<String> flowHandler(Exception e) {
        AlarmWrapper.getInstance().sendMsg("NPE","流量较大请注意观察",e.getMessage());
        return R.fail("系统繁忙请稍后再试!");
    }


    @ExceptionHandler(DegradeException.class)
    public R<String> degradeHandler(Exception e) {
        AlarmWrapper.getInstance().sendMsg("NPE","上游服务出现问题请及时联系",e.getMessage());

        return R.fail("系统开小差，稍后再试!");
    }

    // 权限规则，全局异常处理
    @ExceptionHandler(AuthorityException.class)
    public R<String> handlerAuthorityException() {
        return R.fail("没有权限访问");
    }
    //sentinel+全局异常处理+监控预警
    @ExceptionHandler(Exception.class)
    public R<String> handlerException(Exception e){
        AlarmWrapper.getInstance().sendMsg("NPE","订单服务出现异常",e.getMessage());
        return R.fail(e.getMessage());
    }
}
