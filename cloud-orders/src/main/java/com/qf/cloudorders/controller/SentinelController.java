package com.qf.cloudorders.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.qf.cloudentity.entity.JiFen;
import com.qf.cloudentity.entity.R;
import com.qf.cloudorders.api.JiFenApi;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@RestController
@RequestMapping("sen")
public class SentinelController {

    @Resource
    JiFenApi jiFenApi;

    @GetMapping("test1")
    public String test1() {
        return "Sentinel Test1";
    }


    @GetMapping("test2")
    public String test2() {
        return "Sentinel Test2";
    }


    // 演示链路限流
    @GetMapping("test3")
    public Map<String, Object> test3() {
        System.out.println("演示链路限流 Test3");
        return jiFenApi.save(new JiFen(11,100,"注册送积分"));
    }

    @GetMapping("test4")
    public Map<String, Object> test4() {
        System.out.println("演示链路限流 Test4");
        return jiFenApi.save(new JiFen(11,100,"注册送积分"));
    }


    //慢调用比例
    @GetMapping("test5")
    public Map<String, Object> test5() {
        System.out.println("演示熔断降级 Test5");
        return jiFenApi.save(new JiFen(11,100,"注册送积分"));
    }

    //演示热点Key的限流
    //SentinelResource  指定资源名称
    @GetMapping("test6")
    @SentinelResource("test6-hotkey")
    public String test6(String name){
        System.out.println("传递的参数是:" + name);
        return name;

    }

    //测试网关过滤器
    @GetMapping("test12")
    public R<String> test12(@RequestHeader("name") String name, @RequestParam("age")Integer age){
        return R.ok("name="+name+",age="+age);
    }

}
