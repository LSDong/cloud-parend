package com.qf.cloudorders.controller;

import com.qf.cloudentity.entity.Goods;
import com.qf.cloudentity.entity.JiFen;
import com.qf.cloudentity.entity.R;
import com.qf.cloudentity.entity.TbOrder;
import com.qf.cloudorders.api.JiFenApi;
import com.qf.cloudorders.service.OrdersService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("orders")
public class OrderController {

    @Resource
    RestTemplate restTemplate;

    @Resource
    JiFenApi jiFenApi;

    @Resource
    OrdersService ordersService;

    //下订单接口
    @RequestMapping("save")
    public Map<String, Object> saveOrder(Integer goodsId){
        //想获取商品信息
        //cloud-goods 要调用的服务的服务名称
        String url="http://cloud-goods/goods/findById/"+goodsId;
        //调用商品服务查询商品
        Goods goods = restTemplate.getForObject(url, Goods.class);
        //获取到的商品信息是
        System.out.println(goods);
        System.out.println("开始进行下单操作，保存订单到数据库");
        return new HashMap<String,Object>(){{
            put("code",200);
            put("msg","下单成功");
        }};
    }

    //下订单的接口
    @RequestMapping("saveJiFen")
    public Map<String,Object> saveOrderWithJiFen(Integer goodsId){
        JiFen jiFen = new JiFen();
        jiFen.setJiFenId(1);
        jiFen.setCount(10);
        jiFen.setType("下单送积分");
        Map<String, Object> map = jiFenApi.save(jiFen);
        return map;
    }

    //下单接口
    @PostMapping("saveOrder")
    public R saveOrder(TbOrder order){
        //校验下单数量>0
        return ordersService.saveOrder(order);
    }
}
