package com.qf.cloudorders.dao;

import com.qf.cloudentity.entity.TbOrder;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderDao {

    int insert(TbOrder order);
}
