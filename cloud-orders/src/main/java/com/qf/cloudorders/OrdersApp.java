package com.qf.cloudorders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableDiscoveryClient
//basePackages 要扫描Feign接口所在的包
@EnableFeignClients(basePackages = "")
public class OrdersApp {
    public static void main(String[] args) {
        SpringApplication.run(OrdersApp.class,args);
    }
    @Bean
    @LoadBalanced
    //让ribbon拦截RestTemplate发出的所有请求
    //ribbon获取url中的 service name
    //从nacos注册中下获取实例列表
    //负责从实例列表中通过相应的负载均衡算法,获取一个实例
    //RestTemplate请求实例
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
