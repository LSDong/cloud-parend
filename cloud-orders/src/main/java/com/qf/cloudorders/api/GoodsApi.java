package com.qf.cloudorders.api;

import com.qf.cloudentity.entity.R;
import com.qf.cloudentity.entity.TbGoods;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient("cloud-goods")
@RequestMapping("goods")
public interface GoodsApi {
    //更新商品库存的方法
    @PostMapping("updateStock")
    R updateStock(@RequestParam("goodsId")Integer goodsId,@RequestParam("num")Integer num);

    @GetMapping("{id}")
    R<TbGoods> findByGoodsId(@PathVariable Integer id);
}
