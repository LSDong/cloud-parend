package com.qf.cloudorders.api;

import com.qf.cloudentity.entity.JiFen;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@FeignClient("cloud-jifen")
@RequestMapping("jifen")
public interface JiFenApi {

    @PostMapping(value = "save")
    Map<String,Object> save(@RequestBody JiFen jiFen);
}
