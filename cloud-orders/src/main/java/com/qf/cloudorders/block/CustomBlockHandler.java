package com.qf.cloudorders.block;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.qf.cloudentity.entity.R;

// 自定义的限流异常处理类
public class CustomBlockHandler {

    // 异常处理方法必须是静态方法
    // BlockException异常对象必须添加
    public static R<String> test8(String name,BlockException e) {
        return R.fail("系统繁忙，请稍后再试！");
    }
}
