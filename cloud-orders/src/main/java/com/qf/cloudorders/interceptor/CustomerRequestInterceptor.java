package com.qf.cloudorders.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.stereotype.Component;

// 调用方 要设置请求头
@Component
public class CustomerRequestInterceptor implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate requestTemplate) {
        // 统一给请求头中添加服务名称
        requestTemplate.header("source", "cloud-orders");
    }
}
