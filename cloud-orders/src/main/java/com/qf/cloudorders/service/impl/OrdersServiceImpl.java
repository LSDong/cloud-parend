package com.qf.cloudorders.service.impl;

import com.qf.cloudentity.entity.R;
import com.qf.cloudentity.entity.TbGoods;
import com.qf.cloudentity.entity.TbOrder;
import com.qf.cloudentity.utils.RedissonUtils;
import com.qf.cloudorders.api.GoodsApi;
import com.qf.cloudorders.dao.OrderDao;
import com.qf.cloudorders.service.OrdersService;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrdersServiceImpl implements OrdersService {

    final OrderDao orderDao;
    final GoodsApi goodsApi;

    @Transactional(rollbackFor = Exception.class)
    @Override
    @GlobalTransactional
    public R saveOrder(TbOrder order) {
        //获取分布式锁对象
        RLock rLock = RedissonUtils.getLock("GOODS" + order.getGoodsId());
        try {
            //尝试获取分布式锁：超时时间和时间单位
            if (rLock.tryLock(10, TimeUnit.SECONDS)){
                //下单的业务逻辑
                //1.获取商品ID
                Integer goodsId = order.getGoodsId();
                //2.远程调用商品服务查询商品对象
                R<TbGoods> goodsR = goodsApi.findByGoodsId(goodsId);
                //3.判断商品的最新库存够不够
                if (Objects.equals(goodsR.getCode(),R.SUCCESS)){
                    TbGoods goods = goodsR.getData();
                    //数据库中商品
                    Integer goodsStock = goods.getGoodsStock();
                    //用户的下单数量
                    Integer orderNum = order.getOrderNum();
                    if (goodsStock>orderNum){
                        //可以下单
                        //1.新增订单
                        //订单的总金额=商品单价*下单数量
                        double orderAmount = goods.getGoodsPrice() * orderNum;
                        order.setOrderAmount(orderAmount);
                        order.setOrderId(UUID.randomUUID().toString().replace("-",""));
                        if (orderDao.insert(order)>0){
                            //2.修改库存
                            goodsApi.updateStock(goodsId,-orderNum);
                            //模拟异常：出现异常后本地事务会回滚，但是远程服务不能回滚事务
                            //int a=1/0;
                            return R.ok("下单成功");
                        }else {
                            return R.fail("下单失败");
                        }
                    }else {
                        return R.fail("商品库存不足，不能下单");
                    }
                }else {
                    return R.fail("调用商品服务出错");
                }

            }else {
                return R.fail("获取锁失败");
            }

        }catch (Exception e){
            e.printStackTrace();
            return R.fail("系统故障");
        }finally {
            rLock.unlock();
        }


    }
}
