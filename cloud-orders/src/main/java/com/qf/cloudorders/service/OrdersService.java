package com.qf.cloudorders.service;

import com.qf.cloudentity.entity.R;
import com.qf.cloudentity.entity.TbGoods;
import com.qf.cloudentity.entity.TbOrder;

public interface OrdersService {
    //下单的方法
    R saveOrder(TbOrder order);
}
