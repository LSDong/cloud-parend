package com.qf.cloudgoods.dao;

import com.qf.cloudentity.entity.TbGoods;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface GoodsDao {

    /**
     * 更新商品库存
     * 一个方法适用于：
     * 1、下单时减库存(传递负数的修改的库存数量)
     * 2、取消订单时要回退库存(传递一个正的num)
     *
     * @param goodsId 商品id
     * @param num     修改的库存数量
     * @return int
     */
    int updateStock(@Param("goodsId")Integer goodsId,@Param("num") Integer num);

    TbGoods findById(Integer goodsId);
}
