package com.qf.cloudgoods.service;

import com.qf.cloudentity.entity.R;
import com.qf.cloudentity.entity.TbGoods;

public interface GoodsService {

    R updateStock(Integer goodsId,Integer num);

    R<TbGoods> findById(Integer id);
}
