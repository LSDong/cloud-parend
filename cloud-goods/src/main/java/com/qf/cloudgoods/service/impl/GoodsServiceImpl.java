package com.qf.cloudgoods.service.impl;

import com.qf.cloudentity.entity.R;
import com.qf.cloudentity.entity.TbGoods;
import com.qf.cloudgoods.dao.GoodsDao;
import com.qf.cloudgoods.service.GoodsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class GoodsServiceImpl implements GoodsService {

    final GoodsDao goodsDao;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public R updateStock(Integer goodsId, Integer num) {
        log.info("更新{}商品，修改的库存数是{}",goodsId,num);
        if (goodsDao.updateStock(goodsId,num)>0){
            return R.ok();
        }
        return R.fail();
    }

    @Override
    public R<TbGoods> findById(Integer goodsId) {
        return R.ok(goodsDao.findById(goodsId));
    }


}
