package com.qf.cloudgoods.controller;

import com.qf.cloudentity.entity.Goods;
import com.qf.cloudentity.entity.R;
import com.qf.cloudentity.entity.TbGoods;
import com.qf.cloudgoods.service.GoodsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("goods")
@RequiredArgsConstructor
public class GoodsController {

    final GoodsService goodsService;
    @GetMapping("findById/{id}")
    public Object findById(@PathVariable Integer id) {
        return new Goods(id, "小米手机", 699);
    }

    @PostMapping("updateStock")
    public R updateStock(@RequestParam("goodsId")Integer goodsId,@RequestParam("num")Integer num){
        //需要进行参数校验
        return goodsService.updateStock(goodsId, num);
    }

    @GetMapping("{id}")
    public R<TbGoods> findByGoodsId(@PathVariable Integer id){
        return goodsService.findById(id);
    }
}
