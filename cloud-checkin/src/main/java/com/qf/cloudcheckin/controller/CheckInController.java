package com.qf.cloudcheckin.controller;

import com.qf.cloudcheckin.service.CheckInService;
import com.qf.cloudentity.entity.R;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("checkIn")
public class CheckInController {

    final CheckInService checkInService;

    //用户签到
    @GetMapping
    public R<String> checkIn(Long userId){
        return checkInService.checkIn(userId);
    }

    @GetMapping("continues")
    public R getContinuesDay(Long userId){
        return checkInService.getContinuousCheckIn(userId);
    }

    //获取指定日期的签到人数
    @GetMapping("count")
    public R getCountByDay(String day){
        return checkInService.countDateCheckIn(day);
    }

    //获取指定人员指定时间段的签到次数
    @PostMapping("countByUserId")
    public R countByUserId(Long userId,String startDate,String endDate){
        return checkInService.countCheckIn(userId,startDate,endDate);
    }
}
