package com.qf.cloudcheckin.service;

import com.qf.cloudentity.entity.R;

public interface CheckInService {

    /**
     * 用户签到
     * @param userId 用户ID
     * @return
     */
    R<String> checkIn(Long userId);


    /**
     * 统计指定日期的签到人数
     *
     * @param date 日期
     * @return 指定日期的签到人数
     */
    R<Long> countDateCheckIn(String date);

    /**
     * 数检查
     *获取指定人员在某个时间段的签到人数
     * @param userId    用户id
     * @param startDate 开始日期
     * @param endDate   结束日期
     * @return {@link R}<{@link Long}>
     */
    R<Long> countCheckIn(Long userId,String startDate,String endDate);


    /**
     * 得到连续签到次数
     *
     * @param userId 用户id
     * @return 连续签到次数
     */
    R<Long> getContinuousCheckIn(Long userId);
}
