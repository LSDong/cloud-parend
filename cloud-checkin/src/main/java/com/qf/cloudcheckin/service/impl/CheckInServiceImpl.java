package com.qf.cloudcheckin.service.impl;

import com.qf.cloudcheckin.service.CheckInService;
import com.qf.cloudentity.entity.R;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class CheckInServiceImpl implements CheckInService {

    static final String CHECKIN_PREFIX="USER:CHECKIN:DAY:";

    //用户连续签到的缓存key的前缀=固定前缀字串+用户ID
    static final String CONTINUOUS_CHECK_IN_COUNT_PRE_KEY="USER:CHECKIN:CONTINUOUS_COUNT:";

    static final DateTimeFormatter pattern=DateTimeFormatter.ofPattern("yyyyMMdd");

    final StringRedisTemplate stringRedisTemplate;

    /**
     * 用户签到的接口
     * setbit key offset 0|1
     *
     * setbit USER:CHECKIN:DAY:20230627 userid 1
     * @param userId 用户id
     * @return 签到是否成功
     */
    @Override
    public R<String> checkIn(Long userId) {
        //获取当前日期
        String day = LocalDate.now().format(pattern);
        String key=getCheckInKey(day);
        //判断用户是否签到过
        if (isCheckIn(key,userId)){
            return R.ok("您已经签到过，无需重复签到");
        }
        //进行用户签到
        stringRedisTemplate.opsForValue().setBit(key,userId,true);
        //更新连续签到天数
        updateContinueCheckInDay(userId);
        return R.ok("签到成功");
    }

    /**
     * 统计指定日期的签到人数
     *
     * @param date 日期 yyyyMMdd
     * @return 签到人数
     */
    @Override
    public R<Long> countDateCheckIn(String date) {
        String key = getCheckInKey(date);
        Long count = stringRedisTemplate.execute((RedisCallback<Long>) conn -> conn.bitCount(key.getBytes()));
        return R.ok(count);
    }
    //统计指定用户在指定时间内的签到次数
    //startDate 2023601
    //endDate 2023802
    //getbit 20230601 userid=>0|1

    @Override
    public R<Long> countCheckIn(Long userId, String startDate, String endDate) {
        //获取开始时间和结束时间之间的日期
        LocalDate startLocalDate = LocalDate.parse(startDate, pattern);
        LocalDate endLocalDate = LocalDate.parse(endDate, pattern);
        long distance = ChronoUnit.DAYS.between(startLocalDate, endLocalDate);
        AtomicLong count = new AtomicLong(0L);
        Stream.iterate(startLocalDate,day->day.plusDays(1)).limit(distance+1).forEach(day->{
            String dayStr = day.format(pattern);
            String key = getCheckInKey(dayStr);
            Boolean checkIn = stringRedisTemplate.opsForValue().getBit(key, userId);
            if (Boolean.TRUE.equals(checkIn)){
                count.incrementAndGet();
            }
        });

        return R.ok(count.get());
    }

    /**
     * 得到连续签到天数
     *
     * @param userId 用户id
     * @return {@link R}<{@link Long}>
     */
    @Override
    public R<Long> getContinuousCheckIn(Long userId) {
        String count = stringRedisTemplate.opsForValue().get(CONTINUOUS_CHECK_IN_COUNT_PRE_KEY + userId);
        return R.ok(Long.parseLong(count));
    }

    //获取签到的key 拼接了固定的前缀
    private String getCheckInKey(String key){
        return CHECKIN_PREFIX+key;
    }

    //判断用户是否签到过
    //true:签到过 false:未签到
    private Boolean isCheckIn(String key,Long userId){
        return stringRedisTemplate.opsForValue().getBit(key,userId);
    }

    //更新用户连续签到天数
    private void updateContinueCheckInDay(Long userId){
        //获取用户连续签到天数
        String key = CONTINUOUS_CHECK_IN_COUNT_PRE_KEY + userId;
        String val = stringRedisTemplate.opsForValue().get(key);
        int count = 0;
        if (Objects.nonNull(val)){
            count=Integer.parseInt(val);
        }
        count++;
        //设置连续签到天数
        stringRedisTemplate.opsForValue().set(key,String.valueOf(count));
        //设置连续签到key的过期时间  过期时间为后天的00:00:00
        LocalDateTime expireTime = LocalDateTime.now().plusDays(2).withHour(0).withMinute(0).withSecond(0);
        //设置key的过期
        stringRedisTemplate.expireAt(key,expireTime.toInstant(ZoneOffset.of("+8")));
    }
}
