package com.qf.cloudcheckin.config;

import com.qf.cloud.alarm.core.AlarmWrapper;
import com.qf.cloudentity.entity.R;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.ArrayList;
import java.util.List;

/**
 * 全局异常处理器
 *
 * @author Administrator
 * @date 2023/06/28
 */
@RestControllerAdvice
public class MyControllerAdvice {

    @ExceptionHandler(Exception.class)
    public R<String> handler(Exception e) {
        // 出现异常后 发送预警信息
        // AlarmWrapper.getInstance().sendMsg("NPE", e.getMessage());
//        List<String> users = new ArrayList<>();
//        users.add("1712725320@qq.com");
//        users.add("1326753594@qq.com");
//        AlarmWrapper.getInstance().sendMsgToUser("NPE",e.getMessage(),users);
        AlarmWrapper.getInstance().sendMsg("NPE","异常预警",e.getMessage());
        return R.fail(e.getMessage());
    }
}
